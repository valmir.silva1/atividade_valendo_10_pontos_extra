#!/bin/bash

# Função para exibir a i-ésima linha de um arquivo
linha() {
    local num_linha=$1
    local arquivo=$2

    # Verifica se o arquivo existe
    if [ ! -f "$arquivo" ]; then
        echo "O arquivo '$arquivo' não existe."
        return 1
    fi

    # Exibe a i-ésima linha do arquivo
    sed -n "${num_linha}p" "$arquivo"
}

# Função para exibir a i-ésima coluna de um arquivo (usando ':' como separador)
coluna() {
    local num_coluna=$1
    local arquivo=$2

    # Verifica se o arquivo existe
    if [ ! -f "$arquivo" ]; then
        echo "O arquivo '$arquivo' não existe."
        return 1
    fi

    # Exibe a i-ésima coluna do arquivo usando ':' como separador
    awk -F: -v col="$num_coluna" '{print $col}' "$arquivo"
}

# Listar arquivos no diretório atual
echo "Arquivos no diretório atual:"
ls -l

# Solicitar nome do arquivo
echo "Digite o nome do arquivo:"
read nome_arquivo

# Verificar se o arquivo existe
if [ ! -f "$nome_arquivo" ]; then
    echo "O arquivo '$nome_arquivo' não existe."
    exit 1
fi

# Menu para escolher entre linhas ou colunas
echo "Escolha uma opção:"
echo "1. Exibir linhas"
echo "2. Exibir colunas"
read opcao

case $opcao in
    1)
        # Solicitar número da linha ao usuário
        echo "Digite o número da linha que deseja visualizar:"
        read num_linha
        linha "$num_linha" "$nome_arquivo"  # Chama a função 'linha' com o número da linha digitado
        ;;
    2)
        # Solicitar número da coluna ao usuário
        echo "Digite o número da coluna que deseja visualizar:"
        read num_coluna
        coluna "$num_coluna" "$nome_arquivo"  # Chama a função 'coluna' com o número da coluna digitado
        ;;
    *)
        echo "Opção inválida."
        ;;
esac

